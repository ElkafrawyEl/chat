package apps.elkafrawyel.com.chat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class Chatting_Room extends AppCompatActivity {

    private String MyFriendID;
    private String OurRoomID;
    DatabaseReference mDatabase;
    StorageReference mStorage;
    FirebaseAuth mAuth;
    @BindView(R.id.ChatList)
    RecyclerView ChatList;
    @BindView(R.id.ImageSend)
    ImageView Send;
    @BindView(R.id.ImageAdd)
    ImageView Add;
    @BindView(R.id.imageAddPhoto)
    ImageView AddPhoto;
    @BindView(R.id.imageAddSound)
    ImageView AddSound;
    @BindView(R.id.imageAddVideo)
    ImageView AddVideo;
    @BindView(R.id.txtMessage)
    EditText text;
    @BindView(R.id.mChatToolbar)
    Toolbar Toolbar;
    @BindView(R.id.FileUploadSeekBar)
    SeekBar FileUploadSeekBar;
    @BindView(R.id.Adds)
    LinearLayout Adds;



    MessagesAdapter adapter;
    ArrayList<message> Data;
    private final int Photo_REQUEST=500;
    private final int Sound_REQUEST=600;
    private final int Video_REQUEST=700;

    private final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting_room);
        ButterKnife.bind(this);

        commonMethods.GoOnline();
        ChatList.setHasFixedSize(true);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        ChatList.setLayoutManager(mLinearLayoutManager);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);
        mStorage= FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        MyFriendID = getIntent().getStringExtra("ID");
        OurRoomID=getIntent().getStringExtra("RoomID");
        setSupportActionBar(Toolbar);
        final ActionBar mChatActionBar = getSupportActionBar();
        assert mChatActionBar != null;
        mChatActionBar.setDisplayHomeAsUpEnabled(true);
        mChatActionBar.setDisplayShowCustomEnabled(true);


        //Getting My Friend Data For Custom Action Bar
        mDatabase.child("Users").child(MyFriendID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                                String Name = dataSnapshot.child("name").getValue().toString();
                                String ImageUrl = dataSnapshot.child("image").getValue().toString();
                                String Active = dataSnapshot.child("active").getValue().toString();
                                Boolean Typing = (Boolean) dataSnapshot.child("typing").getValue();
                                LayoutInflater layoutInflater = (LayoutInflater) getSystemService
                                        (LAYOUT_INFLATER_SERVICE);
                                View action_bar_view = layoutInflater.inflate(R.layout.chat_custom_bar, null);
                                mChatActionBar.setCustomView(action_bar_view);
                                TextView Title = action_bar_view.findViewById(R.id.customBarName);
                                TextView LastSeen = action_bar_view.findViewById(R.id.customBarActive);
                                CircleImageView ImageUser = action_bar_view.findViewById(R.id.customBarImage);
                                Title.setText(Name);
                                Glide.with(getApplicationContext()).load(ImageUrl).into(ImageUser);
                                if (Typing) {
                                    LastSeen.setText("Typing....");
                                } else {
                                    if (Active.equals("Yes")) {
                                        LastSeen.setText("Online");
                                    } else {
                                        long Time = Long.parseLong(Active);
                                        LastSeen.setText(getTimeAgo(Time));
                                    }
                                }
                            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(text.getText()))
                    SendText(text.getText().toString());
            }
        });

        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Adds.getVisibility()==View.VISIBLE){
                    Adds.setVisibility(View.GONE);
                }else {
                    Adds.setVisibility(View.VISIBLE);
                }
            }
        });

        AddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(Chatting_Room.this, android.Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(Chatting_Room.this, android.Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Chatting_Room.this, new String[]{android.Manifest
                                        .permission.READ_EXTERNAL_STORAGE}
                                , MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    } else {
                        OpenPhoto();
                    }
                } else {
                    OpenPhoto();
                }
            }
        });

        AddSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(Chatting_Room.this, android.Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(Chatting_Room.this, android.Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Chatting_Room.this, new String[]{android.Manifest
                                        .permission.READ_EXTERNAL_STORAGE}
                                , MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    } else {
                        OpenSound();
                    }
                } else {
                    OpenSound();
                }
            }
        });

        AddVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(Chatting_Room.this, android.Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(Chatting_Room.this, android.Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Chatting_Room.this, new String[]{android.Manifest
                                        .permission.READ_EXTERNAL_STORAGE}
                                , MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    } else {
                        OpenVideo();
                    }
                } else {
                    OpenVideo();
                }
            }
        });

        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("typing")
                .setValue(false);

        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(text.getText())){
                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("typing")
                            .setValue(true);
                }else {
                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("typing")
                            .setValue(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Data=new ArrayList<>();
        adapter=new MessagesAdapter(Data,this,Chatting_Room.this,MyFriendID);
        ChatList.setAdapter(adapter);
        GetOurMessages(OurRoomID);
    }

    private void OpenVideo() {
        Intent intent_upload = new Intent();
        intent_upload.setType("video/*");
        intent_upload.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent_upload,Video_REQUEST);
    }

    private void OpenSound() {
        Intent intent_upload = new Intent();
        intent_upload.setType("audio/*");
        intent_upload.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent_upload,Sound_REQUEST);
    }

    private void OpenPhoto() {
        Intent intent_upload = new Intent();
        intent_upload.setType("image/*");
        intent_upload.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent_upload,Photo_REQUEST);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Press open again", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "we can't access the gallery", Toast.LENGTH_SHORT).show();
                }
            }break;
            case MessagesAdapter.SAVE_PHOTO_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Press Save again", Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(this, "can't save photo", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void GetOurMessages(String ourRoomID) {
        mDatabase.child("Messages").child(ourRoomID).child(MyFriendID).addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot1, String s) {
                        message m=dataSnapshot1.getValue(message.class);
                        Data.add(m);
                        adapter.notifyItemInserted(Data.size()-1);
                        ChatList.scrollToPosition(adapter.getItemCount()-1);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }

    //================================================================================
    private void SendText(final String messageText){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long When = cal.getTimeInMillis();
        final String MessageID = String.valueOf(When);
        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("Room_IDs")
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(MyFriendID)){
                    DatabaseReference Messages=mDatabase.child("Messages").child(OurRoomID).child(MyFriendID).child
                            (MessageID);

                    message m=new message(messageText,"text",mAuth.getCurrentUser().getUid(),MyFriendID,
                            "false",MessageID);
                    Messages.setValue(m);

                    DatabaseReference MyMessages=mDatabase.child("Messages").child(OurRoomID).child
                            (mAuth.getCurrentUser().getUid()).child
                            (MessageID);
                    message m1=new message(messageText,"text",mAuth.getCurrentUser().getUid(),MyFriendID,
                            "true",MessageID);
                    MyMessages.setValue(m1);

                    text.setText(null);

                    //Add This Message To His notification Center
                    FirebaseDatabase.getInstance().getReference().child("Messages Notification Center")
                            .child(MyFriendID).child(OurRoomID).setValue(MessageID);
                    //Add This Message To My notification Center
                    FirebaseDatabase.getInstance().getReference().child("Messages Notification Center")
                            .child(mAuth.getCurrentUser().getUid()).child(OurRoomID).setValue(MessageID);

                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("typing")
                            .setValue(false);
                }else {
                    Toast.makeText(Chatting_Room.this, "You are not friends", Toast
                            .LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    //==================================================================================
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==Photo_REQUEST &&resultCode==RESULT_OK){
            Uri imageUri = data.getData();
            SendImage(imageUri);
        }else if (requestCode==Sound_REQUEST &&resultCode==RESULT_OK){
           Uri SoundUri = data.getData();
           SendSound(SoundUri);
        }else if (requestCode==Video_REQUEST &&resultCode==RESULT_OK){
//            Uri imageUri = data.getData();
//            SendImage(imageUri);
        }
    }

    private void SendImage(Uri imageUri) {
        FileUploadSeekBar.setVisibility(View.VISIBLE);
        Toast.makeText(this, "Sending Image ", Toast.LENGTH_SHORT).show();
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long When = cal.getTimeInMillis();
        final String FileName= String.valueOf(When);

        StorageReference FileRef=mStorage.child("Chat Images").child(OurRoomID).child
                ("DATA_CHAT_"+FileName+"" +
                ".jpg");
        FileRef.putFile(imageUri).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                try {
                    FileUploadSeekBar.setProgress(progress);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull final Task<UploadTask.TaskSnapshot> task) {
                if (task.getResult().getDownloadUrl()!=null){
                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("Room_IDs")
                            .addValueEventListener
                                    (new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.hasChild(MyFriendID)){
                                                mDatabase.child("Messages").child(OurRoomID)
                                                        .child("Photos Names").child(MyFriendID).push()
                                                        .setValue
                                                        ("DATA_CHAT_"+FileName+".jpg");
                                                mDatabase.child("Messages").child(OurRoomID)
                                                        .child("Photos Names").child(mAuth
                                                        .getCurrentUser().getUid()).push()
                                                        .setValue
                                                                ("DATA_CHAT_"+FileName+".jpg");


                                                DatabaseReference Messages=mDatabase.child("Messages").child(OurRoomID).child(MyFriendID).child(FileName);
                                                message m=new message(task.getResult().getDownloadUrl().toString(),"image", mAuth.getCurrentUser().getUid(),MyFriendID, "false",FileName);
                                                Messages.setValue(m);

                                                DatabaseReference MyMessages=mDatabase.child("Messages").child(OurRoomID).child(mAuth.getCurrentUser().getUid()).child(FileName);
                                                message m1=new message(task.getResult().getDownloadUrl().toString(),"image", mAuth.getCurrentUser().getUid(),MyFriendID,"true",FileName);
                                                MyMessages.setValue(m1);

                                                text.setText(null);

                                                //Add This Message To His notification Center
                                                FirebaseDatabase.getInstance().getReference().child("Messages Notification Center")
                                                        .child(MyFriendID).child(OurRoomID).setValue(FileName);
                                                //Add This Message To My notification Center
                                                FirebaseDatabase.getInstance().getReference().child("Messages Notification Center")
                                                        .child(mAuth.getCurrentUser().getUid()).child(OurRoomID).setValue(FileName);

                                                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("typing")
                                                        .setValue(false);

                                                FileUploadSeekBar.setVisibility(View.GONE);
                                            }else {
                                                Toast.makeText(Chatting_Room.this, "You are no longer friends", Toast
                                                        .LENGTH_SHORT).show();
                                                FileUploadSeekBar.setVisibility(View.GONE);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            FileUploadSeekBar.setVisibility(View.GONE);
                                        }
                                    });
                }
            }
        });
    }

    private void SendSound(Uri SoundUri){
        FileUploadSeekBar.setVisibility(View.VISIBLE);
        Toast.makeText(this, "Sending Voice Clip ", Toast.LENGTH_SHORT).show();
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long When = cal.getTimeInMillis();
        final String FileName= String.valueOf(When);

        StorageReference FileRef=mStorage.child("Chat Sounds").child(OurRoomID).child("DATA_Voice_"+FileName);

        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("audio/mpeg")
                .build();

        FileRef.putFile(SoundUri,metadata).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                int progress = (int) ((100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount());
                try {
                    FileUploadSeekBar.setProgress(progress);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull final Task<UploadTask.TaskSnapshot> task) {
                if (task.getResult().getDownloadUrl()!=null){
                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("Room_IDs")
                            .addValueEventListener
                                    (new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.hasChild(MyFriendID)){
                                                mDatabase.child("Messages").child(OurRoomID)
                                                        .child("Sounds Names").child(MyFriendID)
                                                        .push()
                                                        .setValue
                                                                ("DATA_Voice_"+FileName);
                                                mDatabase.child("Messages").child(OurRoomID)
                                                        .child("Sounds Names").child(mAuth
                                                        .getCurrentUser().getUid()).push()
                                                        .setValue("DATA_Voice_"+FileName);


                                                DatabaseReference Messages=mDatabase.child("Messages").child(OurRoomID).child(MyFriendID).child(FileName);
                                                message m=new message(task.getResult()
                                                        .getDownloadUrl().toString(),"voice", mAuth
                                                        .getCurrentUser().getUid(),MyFriendID, "false",FileName);
                                                Messages.setValue(m);

                                                DatabaseReference MyMessages=mDatabase.child("Messages").child(OurRoomID).child(mAuth.getCurrentUser().getUid()).child(FileName);
                                                message m1=new message(task.getResult()
                                                        .getDownloadUrl().toString(),"voice", mAuth
                                                        .getCurrentUser().getUid(),MyFriendID,"true",FileName);
                                                MyMessages.setValue(m1);

                                                text.setText(null);

                                                //Add This Message To His notification Center
                                                FirebaseDatabase.getInstance().getReference().child("Messages Notification Center")
                                                        .child(MyFriendID).child(OurRoomID).setValue(FileName);
                                                //Add This Message To My notification Center
                                                FirebaseDatabase.getInstance().getReference().child("Messages Notification Center")
                                                        .child(mAuth.getCurrentUser().getUid()).child(OurRoomID).setValue(FileName);

                                                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("typing")
                                                        .setValue(false);

                                                FileUploadSeekBar.setVisibility(View.GONE);
                                            }else {
                                                Toast.makeText(Chatting_Room.this, "You are no longer friends", Toast
                                                        .LENGTH_SHORT).show();
                                                FileUploadSeekBar.setVisibility(View.GONE);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            FileUploadSeekBar.setVisibility(View.GONE);
                                        }
                                    });
                }
            }
        });
    }

    //==================================================================================
    //Timing in app
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) { return " active a minute ago"; }
        else if (diff < 2 * MINUTE_MILLIS) {
            return " active a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return "Last Seen "+diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "active an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return "Last Seen "+ diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return " active  since yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        commonMethods.GoOnline();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        commonMethods.GoOnline();
    }
    @Override
    protected void onPause() {
        super.onPause();
        commonMethods.Go_Offline();
    }
    CommonMethods commonMethods=new CommonMethods();
}