package apps.elkafrawyel.com.chat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Mahmoud Ashraf on 1/19/2018.
 */

class CommonMethods {
    void Go_Offline(){
        if (FirebaseAuth.getInstance().getCurrentUser()!=null){
            FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                    .getInstance().getCurrentUser().getUid())
                    .child("active")
                    .setValue(String
                            .valueOf(System.currentTimeMillis()));
        }
    }

    void GoOnline(){
        if (FirebaseAuth.getInstance().getCurrentUser()!=null){
            FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                    .getInstance().getCurrentUser().getUid())
                    .child("active")
                    .setValue("Yes");
        }
    }
}
