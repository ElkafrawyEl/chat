package apps.elkafrawyel.com.chat;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FriendsFragment extends Fragment {

    @BindView(R.id.FriendsList)RecyclerView FriendsList;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.mySearchFAB)
    FloatingActionButton mySearchFAB;
    DatabaseReference mDatabase;
    String mOnlineUser;
    FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View FriendsPage= inflater.inflate(R.layout.fragment_friends, container, false);
        ButterKnife.bind(this,FriendsPage);

        mAuth=FirebaseAuth.getInstance();
        mOnlineUser=mAuth.getCurrentUser().getUid();
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Friends");
        mDatabase= FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);
        FriendsList.setHasFixedSize(true);
        FriendsList.setLayoutManager(new LinearLayoutManager(container.getContext()));
        MyFriends(container.getContext());
        mySearchFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenSearchDialog(container.getContext());
            }
        });
        return FriendsPage;
    }
    FirebaseRecyclerAdapter<Object, MyFriendsViewHolder> MyFriends_Adapter;
    private void MyFriends(final Context context){
        MyFriends_Adapter=new FirebaseRecyclerAdapter<Object, MyFriendsViewHolder>(
                Object.class,
                R.layout.my_friend_row,
                MyFriendsViewHolder.class,
                mDatabase.child("Friends").child(mOnlineUser)
        ) {
            @Override
            public MyFriendsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getContext())
                        .inflate(R.layout.my_friend_row, parent, false);
                return new MyFriendsViewHolder(view);
            }

            @Override
            protected void populateViewHolder(final MyFriendsViewHolder viewHolder, Object model, int position) {
                if (model != null) {
                    final String MyFriendID = getRef(position).getKey();
                    viewHolder.Set_User(MyFriendID,context);

                    viewHolder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CharSequence[] Options = new CharSequence[]{"Open Profile","Send Message"};
                            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Select Options");
                            builder.setItems(Options, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (i == 0) {
                                        //Quick profile
                                        if (!MyFriendID.equals(mOnlineUser))
                                            OpenQuickProfile(MyFriendID,getContext());
                                    }
                                    if (i == 1) {
                                        //Open Chat
                                        //Open Chat
                                        if (!MyFriendID.equals(mOnlineUser))
                                            CreateRoomForUs(MyFriendID);
                                    }
                                }
                            });
                            builder.show();
                        }
                    });
                }
            }
        };
        MyFriends_Adapter.notifyDataSetChanged();
        FriendsList.setAdapter(MyFriends_Adapter);
    }
    public static class MyFriendsViewHolder extends RecyclerView.ViewHolder {
        View view;
        CircleImageView User_Image;
        TextView User_Name;
        ImageView ImageActive;
        MyFriendsViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            User_Image = view.findViewById(R.id.ImageMyFriendIcon);
            User_Name = view.findViewById(R.id.txtMyFriendName);
            ImageActive=view.findViewById(R.id.ImageActive);
        }
        void Set_User(String ID, final Context context) {
            FirebaseDatabase.getInstance().getReference().child("Users").child(ID).addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String Name= (String) dataSnapshot.child("name").getValue();
                            if (Name != null)
                                User_Name.setText(Name);


                            String Url= (String) dataSnapshot.child("image").getValue();
                            if (Url != null&&context!=null)
                                Glide.with(context.getApplicationContext()).load(Url).into(User_Image);

                            String Active= (String) dataSnapshot.child("active").getValue();
                            if (Active!=null) {
                                if (Active.equals("Yes"))
                                    setActive();
                                else
                                    setOffline();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );
        }

        void setActive()
        {
            ImageActive.setVisibility(View.VISIBLE);
        }
        void setOffline()
        {
            ImageActive.setVisibility(View.GONE);
        }

    }
    private void OpenChat(final String MyFriendID,String RoomID){
        Intent i=new Intent(getContext(),Chatting_Room.class);
        i.putExtra("ID",MyFriendID);
        i.putExtra("RoomID",RoomID);
        startActivity(i);
    }
    //===============================================================================
    //if we talk before ??
    private void CreateRoomForUs(final String ID){
        mDatabase.child("Friends").child(mOnlineUser).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(ID)){
                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    //Chat Room Exists
                                    if (dataSnapshot.child("Room_IDs").hasChild(ID)){
                                        //Open Chat with him
                                        String RoomID=dataSnapshot.child("Room_IDs").child(ID).getValue().toString();
                                        OpenChat(ID,RoomID);
                                    }else {
                                        //Chat Room Not Exists
                                        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                                        long When = cal.getTimeInMillis();

                                        String NewRoom=String.valueOf(When);

                                        //Adding To Me
                                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid())
                                                .child("Room_IDs").child(ID).setValue(NewRoom);

                                        //Adding To Him
                                        mDatabase.child("Users").child(ID)
                                                .child("Room_IDs").child(mAuth.getCurrentUser().getUid()).setValue(NewRoom);

                                        OpenChat(ID,NewRoom);

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }else
                    Toast.makeText(getContext(), "You have to be friends first ", Toast
                            .LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void OpenQuickProfile(final String ID, final Context context) {
        final CircleImageView ImageUser;
        final ImageView ImgActive;
        final TextView UserName;
        final Button btnOperations;
        final Dialog myDialog = new Dialog(getContext());
        myDialog.setContentView(R.layout.quick_profile_dialog);
        myDialog.show();
        ImageUser=myDialog.findViewById(R.id.ImageUserProfileQuick);
        ImgActive=myDialog.findViewById(R.id.ImageActiveProfileQuick);
        UserName=myDialog.findViewById(R.id.txtUserNameProfileQuick);
        btnOperations=myDialog.findViewById(R.id.btnOperations);

            FirebaseDatabase.getInstance().getReference().child("Users").child(ID).addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String Name= (String) dataSnapshot.child("name").getValue();
                            if (Name != null)
                                UserName.setText(Name);

                            String Url= (String) dataSnapshot.child("image").getValue();
                            if (Url != null)
                                Glide.with(context).load(Url).into(ImageUser);

                            String Active= (String) dataSnapshot.child("active").getValue();
                            if (Active!=null) {
                                if (Active.equals("Yes"))
                                    ImgActive.setVisibility(View.VISIBLE);
                                else
                                    ImgActive.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );
        //===========================================================================
        //if i sent him a request
        mDatabase.child("Rec_Friends").child(ID).child(mOnlineUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    btnOperations.setText(R.string.cancel_request);
                    btnOperations.setTag("cancel");
                } else {
                    btnOperations.setText(R.string.send_friend_request);
                    btnOperations.setTag("send");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //if i friend with him
        mDatabase.child("Friends").child(ID).child(mOnlineUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    btnOperations.setText(R.string.send_friend_request);
                    btnOperations.setTag("send");
                } else {
                    btnOperations.setText(R.string.un_friend);
                    btnOperations.setTag("un");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //=============================================================================
        btnOperations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnOperations.getTag().equals("cancel")){
                    mDatabase.child("Friends_Req").child(ID).child
                            (mOnlineUser).removeValue();
                    btnOperations.setText(R.string.send_friend_request);
                    btnOperations.setTag("send");
                    mDatabase.child("FriendShip " +
                            "Notification").child(ID)
                            .child(mOnlineUser).setValue(null);
                }else if (btnOperations.getTag().equals("send")){
                    mDatabase.child("Friends_Req").child(ID).child(mOnlineUser).setValue(String.valueOf(System.currentTimeMillis()));
                    btnOperations.setText(R.string.cancel_request);
                    btnOperations.setTag("cancel");

                    //I Send him a request store into his
                    // notifications
                    mDatabase.child("FriendShip Notification").child(ID)
                            .child(mOnlineUser).setValue(System.currentTimeMillis()+",req");

                }else if(btnOperations.getTag().equals("un")){
                    mDatabase.child("Friends").child(ID).child(mOnlineUser).removeValue();
                    mDatabase.child("Friends").child(mOnlineUser).child(ID).removeValue();
                    btnOperations.setText(R.string.send_friend_request);
                    btnOperations.setTag("send");
                    //I Send him a request store into his
                    // notifications
                    mDatabase.child("FriendShip Notification").child(ID)
                            .child(mOnlineUser).setValue(System.currentTimeMillis()+",un");

                    //Delete Chat between us
                    DeleteChat(mOnlineUser,ID);
                }
            }
        });


        //=============================================================================
        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = myDialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
    private void DeleteChat(final String mOnlineUser, final String mFriendID) {
        //Getting OurRoom and delete messages
        //From My Side only
        mDatabase.child("Users").child(mOnlineUser).child("Room_IDs").child(mFriendID).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue()!=null){
                            final String OurRoomID=dataSnapshot.getValue().toString();
                            //This will remove message from my ref and his ref
                            mDatabase.child("Messages").child(OurRoomID).child(mOnlineUser).removeValue();
                            //mDatabase.child("Messages").child(OurRoomID).child(mFriendID).removeValue();
                            //mDatabase.child("Messages Notification Center").child(mFriendID).child(OurRoomID).removeValue();
                            mDatabase.child("Messages Notification Center").child(mOnlineUser).child(OurRoomID).removeValue();


                            mDatabase.child("Messages").child(OurRoomID)
                                    .child("Photos Names")
                                    .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.child(mFriendID).getChildrenCount()>0){
                                        mDatabase.child("Messages").child(OurRoomID)
                                                .child("Photos Names").child(mAuth
                                                .getCurrentUser().getUid()).removeValue();
                                    }else {
                                        mDatabase.child("Messages").child(OurRoomID).child("Photos Names").child(mAuth
                                                .getCurrentUser().getUid()).removeValue();

                                        for (DataSnapshot name:dataSnapshot.child(mAuth.getCurrentUser().getUid()).getChildren()){
                                            String PhotoName=name.getValue().toString();
                                            //Delete photos
                                            FirebaseStorage.getInstance().getReference().child("Chat Images")
                                                    .child(OurRoomID).child(PhotoName).delete();
                                        }
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });

                            mDatabase.child("Messages").child(OurRoomID)
                                    .child("Sounds Names")
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.child(mFriendID).getChildrenCount()>0){
                                                mDatabase.child("Messages").child(OurRoomID)
                                                        .child("Sounds Names").child(mAuth
                                                        .getCurrentUser().getUid()).removeValue();
                                            }else {
                                                mDatabase.child("Messages").child(OurRoomID)
                                                        .child("Sounds Names").child(mAuth
                                                        .getCurrentUser().getUid()).removeValue();

                                                for (DataSnapshot name:dataSnapshot.child(mAuth.getCurrentUser().getUid()).getChildren()){
                                                    String PhotoName=name.getValue().toString();
                                                    //Delete photos
                                                    FirebaseStorage.getInstance().getReference()
                                                            .child("Chat Sounds")
                                                            .child(OurRoomID).child(PhotoName).delete();
                                                }
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                }
        );
        //Remove OurRoom
       // mDatabase.child("Users").child(mFriendID).child("Room_IDs").child(mOnlineUser).removeValue();
        mDatabase.child("Users").child(mOnlineUser).child("Room_IDs").child(mFriendID).removeValue();
    }
    public static class UsersViewHolder extends RecyclerView.ViewHolder {
        View view;
        CircleImageView User_Image;
        TextView User_Name;
        Button btnSendReq;
        ImageView ImageActiveSearch;
        UsersViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            User_Image = view.findViewById(R.id.ImageUserIcon);
            User_Name = view.findViewById(R.id.txtName);
            btnSendReq=view.findViewById(R.id.btnSendReq);
            ImageActiveSearch=view.findViewById(R.id.ImageActiveSearch);
        }
        void Set_User(String ID, final Context context) {
            FirebaseDatabase.getInstance().getReference().child("Users").child(ID).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String Name= (String) dataSnapshot.child("name").getValue();
                            if (Name != null)
                                User_Name.setText(Name);


                            String Url= (String) dataSnapshot.child("image").getValue();
                            if (Url != null)
                                Glide.with(context).load(Url).into(User_Image);

                            String Active= (String) dataSnapshot.child("active").getValue();
                            if (Active!=null) {
                                if (Active.equals("Yes"))
                                    setActive();
                                else
                                    setOffline();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );
        }

        void setActive()
        {
            ImageActiveSearch.setVisibility(View.VISIBLE);
        }
        void setOffline()
        {
            ImageActiveSearch.setVisibility(View.GONE);
        }
    }
    FirebaseRecyclerAdapter<users, UsersViewHolder> Search_Recycler;
    public void OpenSearchDialog(Context context){
        final Dialog myDialog = new Dialog(context);
        myDialog.setContentView(R.layout.all_users_dialog);
        myDialog.show();
        android.support.v7.widget.SearchView searchView=myDialog.findViewById(R.id.UsersSearchView);
        final RecyclerView SearchList=myDialog.findViewById(R.id.SearchList);
        SearchList.setHasFixedSize(true);
        SearchList.setLayoutManager(new LinearLayoutManager(context));
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Query Q = mDatabase.child("Users").orderByChild("name")
                        .startAt(newText).endAt(newText +"\uf8ff");
                Search_Recycler = new
                        FirebaseRecyclerAdapter<users, UsersViewHolder>(
                                users.class,
                                R.layout.r_user,
                                UsersViewHolder.class,
                                Q
                        ) {
                            @Override
                            public UsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                                View view = LayoutInflater.from(getContext())
                                        .inflate(R.layout.r_user, parent, false);

                                return new UsersViewHolder(view);
                            }
                            @Override
                            protected void populateViewHolder(final UsersViewHolder viewHolder, final users
                                    model, int position) {
                                if (model != null) {
                                    final String KeyID = getRef(position).getKey();
                                    viewHolder.Set_User(KeyID, getContext());
                                    SearchList.setVisibility(View.VISIBLE);
                                    if (KeyID!=null) {
                                        mDatabase.child("Users").child(KeyID)
                                                .addValueEventListener(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        if (dataSnapshot!=null){
                                                            String Active = (String) dataSnapshot.child("active").getValue();
                                                            assert (Active) != null;
                                                            if ((Active).equals("Yes"))
                                                                viewHolder.setActive();
                                                            else
                                                                viewHolder.setOffline();

                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                    }

                                    assert KeyID != null;
                                    if (KeyID.equals(mOnlineUser))
                                        viewHolder.btnSendReq.setVisibility(View.GONE);


                                    mDatabase.child("Friends_Req").child(KeyID).child(mOnlineUser)
                                            .addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.getValue() == null) {
                                                        viewHolder.btnSendReq.setText(R.string.send_friend_request);
                                                    } else {
                                                        viewHolder.btnSendReq.setText(R.string.cancel_request);
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                    mDatabase.child("Friends").child(KeyID).child(mOnlineUser)
                                            .addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.getValue() != null) {
                                                        viewHolder.btnSendReq.setVisibility(View.GONE);
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });

                                    viewHolder.view.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            CharSequence[] Options = new CharSequence[]{"Open Profile","Send Message"};
                                            final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                            builder.setTitle("Select Options");
                                            builder.setItems(Options, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    if (i == 0) {
                                                        //Quick profile
                                                        if (!KeyID.equals(mOnlineUser))
                                                            OpenQuickProfile(KeyID,getContext());
                                                    }
                                                    if (i == 1) {
                                                        //Open Chat
                                                        if (!KeyID.equals(mOnlineUser))
                                                            CreateRoomForUs(KeyID);
                                                    }
                                                }
                                            });
                                            builder.show();
                                        }
                                    });
                                    viewHolder.btnSendReq.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            mDatabase.child("Friends_Req").child(KeyID).child(mOnlineUser)
                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            if (dataSnapshot.getValue() == null) {
                                                                mDatabase.child("Friends_Req").child(KeyID).child(mOnlineUser).setValue(String.valueOf(System.currentTimeMillis()));
                                                                viewHolder.btnSendReq.setText(R.string.cancel_request);
                                                                viewHolder.btnSendReq.setTag("Get");
                                                                //===================================
                                                                //I Send him a request store into his
                                                                // notifications
                                                                mDatabase.child("FriendShip Notification").child(KeyID)
                                                                        .child(mOnlineUser).setValue
                                                                        (System.currentTimeMillis()+",req");
                                                            } else {
                                                                mDatabase.child("Friends_Req").child(KeyID).child
                                                                        (mOnlineUser).removeValue();
                                                                mDatabase.child("FriendShip " +
                                                                        "Notification").child(KeyID)
                                                                        .child(mOnlineUser).setValue(null);
                                                                viewHolder.btnSendReq.setText(R.string.send_friend_request);
                                                                viewHolder.btnSendReq.setTag("Send");
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });
                                        }
                                    });
                                }
                            }
                        };
                Search_Recycler.notifyDataSetChanged();
                SearchList.setAdapter(Search_Recycler);
                return false;
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = myDialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
