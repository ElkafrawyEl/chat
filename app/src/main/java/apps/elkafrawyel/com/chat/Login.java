package apps.elkafrawyel.com.chat;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Login extends AppCompatActivity {

    @BindView(R.id.txtEmail)TextView txtEmail;
    @BindView(R.id.txtPassword)TextView txtPassword;
    @BindView(R.id.Log_btnLogin)Button Login;
    @BindView(R.id.Log_btnNeedAccount)Button NeedAccount;
    @BindView(R.id.LoginprogressBar)ProgressBar progressBar;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mAuth= FirebaseAuth.getInstance();
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void LoginClick(View view) {
        String Email=txtEmail.getText().toString();
        String Password=txtPassword.getText().toString();
        if (!TextUtils.isEmpty(Email)&& !TextUtils.isEmpty(Password)) {
            if (isNetworkAvailable(this)){
                progressBar.setVisibility(View.VISIBLE);
                mAuth.signInWithEmailAndPassword(Email,Password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    startActivity(new Intent(Login.this,MainActivity.class));
                                    progressBar.setVisibility(View.INVISIBLE);
                                    finish();
                                }else {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(Login.this, "Invalid Email or Password", Toast
                                            .LENGTH_SHORT).show();
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(Login.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }else
                Toast.makeText(Login.this, "No Connection", Toast.LENGTH_SHORT).show();
        }

    }
    public boolean isNetworkAvailable(Context C){
        ConnectivityManager mConnectivity = (ConnectivityManager) C.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert mConnectivity != null;
        NetworkInfo networkInfo = mConnectivity.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }


    public void NeedAccountClick(View view) {
        startActivity(new Intent(Login.this,SingUp.class));
    }
}
