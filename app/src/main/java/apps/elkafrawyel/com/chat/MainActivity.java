package apps.elkafrawyel.com.chat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mHomeNav;
    private ProfileFragment profileFragment;
    private RoomsFragment messagesFragment;
    private FriendsFragment friendsFragment;
    private RequestsFragment requestsFragment;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commonMethods.GoOnline();
        profileFragment=new ProfileFragment();
        messagesFragment=new RoomsFragment();
        friendsFragment=new FriendsFragment();
        requestsFragment=new RequestsFragment();
        setContentView(R.layout.activity_main);
        this.setTitle("Chat App");
        mAuth=FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser()==null){
            startActivity(new Intent(this,Login.class));
            finish();
        }
        mHomeNav= (BottomNavigationView) findViewById(R.id.mHomeNav);
        mHomeNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_account:
                        if (mHomeNav.getSelectedItemId()!=R.id.nav_account){
                            mHomeNav.setItemBackgroundResource(R.color.colorAccent);
                            Bundle bundle=new Bundle();
                            bundle.putString("ID",mAuth.getCurrentUser().getUid());
                            profileFragment.setArguments(bundle);
                            setFragment(profileFragment);
                        }
                        return true;

                    case R.id.nav_friends:
                        if (mHomeNav.getSelectedItemId()!=R.id.nav_friends) {
                            mHomeNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                            setFragment(friendsFragment);

                        }
                        return true;


                    case R.id.nav_messages:
                        if (mHomeNav.getSelectedItemId()!=R.id.nav_messages) {
                            mHomeNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                            setFragment(messagesFragment);
                        }
                        return true;

                    case R.id.nav_ReqFriends:
                        if (mHomeNav.getSelectedItemId()!=R.id.nav_ReqFriends) {
                            mHomeNav.setItemBackgroundResource(R.color.colorPrimaryDark);
                            setFragment(requestsFragment);
                        }
                        return true;
                    default:
                        return false;

                }
            }
        });
        setFragment(friendsFragment);
        mHomeNav.setSelectedItemId(R.id.nav_friends);
        mHomeNav.setItemBackgroundResource(R.color.colorPrimaryDark);
    }

    private void setFragment(Fragment Fragment) {
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.Home_Container,Fragment).commit();
    }



    @Override
    protected void onResume() {
        super.onResume();
        commonMethods.GoOnline();
    }

    @Override
    protected void onPause() {
        super.onPause();
        commonMethods.Go_Offline();
    }
    CommonMethods commonMethods=new CommonMethods();

}
