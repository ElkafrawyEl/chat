package apps.elkafrawyel.com.chat;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import javax.microedition.khronos.opengles.GL;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mahmoud Ashraf on 1/18/2018.
 */

public class MessagesAdapter extends RecyclerView.Adapter {
    ArrayList<message> Data=new ArrayList<>();
    Context context;
    FirebaseAuth mAuth;
    String MyFriend;
    Activity mActivity;
    static final int SAVE_PHOTO_REQUEST=11;
    final String appDirectoryName = "Chat App";
    final File imageRoot = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES), appDirectoryName);

    final File videoRoot = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_MOVIES), appDirectoryName);

    final File soundRoot = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_MUSIC), appDirectoryName);



    public MessagesAdapter(ArrayList<message> data, Context context, Activity mActivity, String
            myFriend) {
        Data = data;
        this.context = context;
        this.mActivity=mActivity;
        MyFriend = myFriend;
        mAuth=FirebaseAuth.getInstance();
    }

    @Override
    public int getItemViewType(int position) {
        message newMessage = Data.get(position);
        String Type=newMessage.getType();

        if (Type.equals("text")){
            if (newMessage.getFrom().equals(mAuth.getCurrentUser().getUid()))
                return 0;
            else
                return 1;
        }else if (Type.equals("image")){
            if (newMessage.getFrom().equals(mAuth.getCurrentUser().getUid()))
                return 2;
            else
                return 3;
        }else {
            if (newMessage.getFrom().equals(mAuth.getCurrentUser().getUid()))
                return 4;
            else
                return 5;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //now viewType works as position
        if (viewType==0) {
            return new TextTo_ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                            .layout.to_layout,
                    parent, false));
        } else if ((viewType==1)){
            return new TextFrom_ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                            .layout.from_layout,
                    parent, false));
        }else if ((viewType==2)){
            return new ImageTo_ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                            .layout.image_to_layout,
                    parent, false));
        }else if ((viewType==3)){
            return new ImageFrom_ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                            .layout.image_from_layout,
                    parent, false));
        }else if ((viewType==4)){
            return new SoundTo_ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                            .layout.sound_to_layout,
                    parent, false));
        }else {
            return new SoundFrom_ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                            .layout.sound_from_layout,
                    parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (Data.size()>0) {
            message newMessage = Data.get(position);
            String To=newMessage.getTo();
            String From=newMessage.getFrom();
            final String Text=newMessage.getMessage();
            String Seen=newMessage.getSeen();
            final String Time=newMessage.getWhen();
            String Type=newMessage.getType();
            Calendar calendar = Calendar.getInstance();
            DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");
            calendar.setTimeInMillis(Long.parseLong(Time));
            String When= dateFormat.format(calendar.getTime());
            switch (Type) {
                case "text":
                    if (Objects.equals(From, mAuth.getCurrentUser().getUid())) {
                        final TextTo_ViewHolder To_holder = (TextTo_ViewHolder) holder;
                        To_holder.txtTo.setText(Text);
                        To_holder.txtWhenTo.setText(When);
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child
                                ("Users").child(mAuth.getCurrentUser().getUid());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String image = dataSnapshot.child("image").getValue().toString();
                                Glide.with(context.getApplicationContext()).load(image).into(To_holder
                                        .ImageUserChatTo);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        To_holder.view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (To_holder.txtWhenTo.getVisibility() == View.VISIBLE) {
                                    To_holder.txtWhenTo.setVisibility(View.GONE);
                                } else {
                                    To_holder.txtWhenTo.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    } else if (Objects.equals(From, MyFriend)) {
                        final TextFrom_ViewHolder From_holder = (TextFrom_ViewHolder) holder;
                        From_holder.txt_From.setText(Text);
                        From_holder.txtWhenFrom.setText(When);
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child
                                ("Users").child(MyFriend);
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String image = dataSnapshot.child("image").getValue().toString();
                                Glide.with(context.getApplicationContext()).load(image).into(From_holder.ImageUserChatFrom);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        From_holder.view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (From_holder.txtWhenFrom.getVisibility() == View.VISIBLE) {
                                    From_holder.txtWhenFrom.setVisibility(View.GONE);
                                } else {
                                    From_holder.txtWhenFrom.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }
                    break;
                case "image":
                    if (Objects.equals(From, mAuth.getCurrentUser().getUid())) {
                        final ImageTo_ViewHolder To_holder = (ImageTo_ViewHolder) holder;
                        Glide.with(context.getApplicationContext()).load(Text).into
                                (To_holder.ImageTo);
                        To_holder.txtImageWhenTo.setText(When);
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child
                                ("Users").child(mAuth.getCurrentUser().getUid());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String image = dataSnapshot.child("image").getValue().toString();
                                Glide.with(context.getApplicationContext()).load(image).into(To_holder
                                        .ImageUserChatTo);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        To_holder.view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Open Full Screen Image
                                Uri imageUri = Uri.parse(Text);
                                OpenFullScreenImage(imageUri, mActivity, Time, context);
                            }
                        });
                    } else if (Objects.equals(From, MyFriend)) {
                        final ImageFrom_ViewHolder From_holder = (ImageFrom_ViewHolder) holder;
                        Glide.with(context.getApplicationContext()).load(Text).into(From_holder.ImageFrom);
                        From_holder.txtImageWhenFrom.setText(When);
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child
                                ("Users").child(MyFriend);
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String image = dataSnapshot.child("image").getValue().toString();
                                Glide.with(context.getApplicationContext()).load(image).into(From_holder.ImageUserChatFrom);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        From_holder.view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Open Full Screen Image
                                Uri imageUri = Uri.parse(Text);
                                OpenFullScreenImage(imageUri, mActivity, Time, context);
                            }
                        });
                    }
                    break;
                case "voice":

                    final Uri VoiceUrl = Uri.parse(Text);
                    if (Objects.equals(From, mAuth.getCurrentUser().getUid())) {
                        final SoundTo_ViewHolder To_holder = (SoundTo_ViewHolder) holder;
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child
                                ("Users").child(mAuth.getCurrentUser().getUid());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String image = dataSnapshot.child("image").getValue().toString();
                                Glide.with(context.getApplicationContext()).load(image).into(To_holder
                                        .ImageUserTo);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                        To_holder.ImagePlayTo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final MediaPlayer mp = MediaPlayer.create(context, VoiceUrl);
                                    To_holder.ImagePlayTo.setEnabled(false);
                                    if (mp != null) {
                                        To_holder.ImagePlayTo.setImageResource(R.drawable.ic_pause_white_24dp);
                                        mp.start();
                                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mediaPlayer) {
                                                To_holder.ImagePlayTo.setImageResource(R.drawable.ic_play_arrow_white_24dp);
                                                mp.stop();
                                                To_holder.ImagePlayTo.setEnabled(true);
                                            }
                                        });
                                    }
                                }
                        });

                    } else if (Objects.equals(From, MyFriend)) {
                        final SoundFrom_ViewHolder From_holder = (SoundFrom_ViewHolder) holder;
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child
                                ("Users").child(mAuth.getCurrentUser().getUid());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String image = dataSnapshot.child("image").getValue().toString();
                                Glide.with(context.getApplicationContext()).load(image).into(From_holder
                                        .ImageUserChatFrom);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                        From_holder.ImagePlayFrom.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final MediaPlayer mp = MediaPlayer.create(context, VoiceUrl);
                                    From_holder.ImagePlayFrom.setEnabled(false);
                                    if (mp != null) {
                                        From_holder.ImagePlayFrom.setImageResource(R.drawable.ic_pause_white_24dp);
                                        mp.start();
                                        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mediaPlayer) {
                                                From_holder.ImagePlayFrom.setImageResource(R.drawable.ic_play_arrow_white_24dp);
                                                mp.stop();
                                                From_holder.ImagePlayFrom.setEnabled(true);
                                            }
                                        });
                                }
                            }
                        });
                    }
                    break;
            }
        }
    }


    private void OpenFullScreenImage(Uri imageUri, final Activity activity, final String Time, final
    Context context) {
        final Dialog myDialog = new Dialog(context);
        myDialog.setContentView(R.layout.fullscreen_image);
        ImageView imageExit=myDialog.findViewById(R.id.imageExit);
        Button SavePhoto=myDialog.findViewById(R.id.SavePhoto);
        final ImageView fullScreenImageView = myDialog.findViewById(R.id.fullScreenImageView);
        if(imageUri != null && fullScreenImageView != null) {
            Glide.with(context.getApplicationContext())
                    .load(imageUri)
                    .into(fullScreenImageView);
        }
        imageExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.dismiss();
            }
        });
        SavePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(context, Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest
                                .permission.READ_EXTERNAL_STORAGE},SAVE_PHOTO_REQUEST);
                    } else {
                        assert fullScreenImageView != null;
                        Bitmap bitmap = ((BitmapDrawable)fullScreenImageView.getDrawable()).getBitmap();
                        saveToInternalStorage(bitmap,Time,context);
                        Toast.makeText(context, "Photo Saved to This Device", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    assert fullScreenImageView != null;
                    Bitmap bitmap = ((BitmapDrawable)fullScreenImageView.getDrawable()).getBitmap();
                    saveToInternalStorage(bitmap,Time,context);
                    Toast.makeText(context, "Photo Saved to This Device", Toast.LENGTH_SHORT).show();
                }

            }
        });

        myDialog.show();
        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = myDialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }


    private void saveToInternalStorage(Bitmap bitmapImage,String Time ,Context context){
        imageRoot.mkdirs();
        final File imagePath = new File(imageRoot, "DATA_CHAT_"+Time+".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imagePath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert fos != null;
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public int getItemCount() {
        return Data.size();
    }


    public class TextFrom_ViewHolder extends RecyclerView.ViewHolder{
        View view;
        TextView txt_From,txtWhenFrom;
        CircleImageView ImageUserChatFrom;
        TextFrom_ViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            txt_From=view.findViewById(R.id.txtFrom);
            txtWhenFrom=view.findViewById(R.id.txtWhenFrom);
            ImageUserChatFrom=view.findViewById(R.id.ImageUserChatFrom);

        }
    }
    public class TextTo_ViewHolder extends RecyclerView.ViewHolder{
        View view;
        TextView txtTo;
        TextView txtWhenTo;
        CircleImageView ImageUserChatTo;
        TextTo_ViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            txtTo=view.findViewById(R.id.txtTo);
            txtWhenTo=view.findViewById(R.id.txtWhenTo);
            ImageUserChatTo=view.findViewById(R.id.ImageUserTo);
        }
    }

    public class ImageTo_ViewHolder extends RecyclerView.ViewHolder{
        View view;
        ImageView ImageTo;
        TextView txtImageWhenTo;
        CircleImageView ImageUserChatTo;
        ImageTo_ViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            ImageTo=view.findViewById(R.id.ImageTo);
            txtImageWhenTo=view.findViewById(R.id.txtWhenImageTo);
            ImageUserChatTo=view.findViewById(R.id.ImageUserTo);
        }
    }


    public class ImageFrom_ViewHolder extends RecyclerView.ViewHolder{
        View view;
        ImageView ImageFrom;
        TextView txtImageWhenFrom;
        CircleImageView ImageUserChatFrom;
        ImageFrom_ViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            ImageFrom=view.findViewById(R.id.ImageFrom);
            txtImageWhenFrom=view.findViewById(R.id.txtImageWhenFrom);
            ImageUserChatFrom=view.findViewById(R.id.ImageUserChatFrom);

        }
    }

    public class SoundTo_ViewHolder extends RecyclerView.ViewHolder {

        View view;
        ImageView ImagePlayTo;
        TextView txtWhenSoundTo;
        CircleImageView ImageUserTo;
        SoundTo_ViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            ImagePlayTo=view.findViewById(R.id.ImagePlayTo);
            txtWhenSoundTo=view.findViewById(R.id.txtWhenSoundTo);
            ImageUserTo=view.findViewById(R.id.ImageUserTo);

        }
    }


    public class SoundFrom_ViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView ImagePlayFrom;
        TextView txtSoundWhenFrom;
        CircleImageView ImageUserChatFrom;
        SoundFrom_ViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            ImagePlayFrom=view.findViewById(R.id.ImagePlayFrom);
            txtSoundWhenFrom=view.findViewById(R.id.txtSoundWhenFrom);
            ImageUserChatFrom=view.findViewById(R.id.ImageUserChatFrom);
        }
    }
}
