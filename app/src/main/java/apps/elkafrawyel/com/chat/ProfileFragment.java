package apps.elkafrawyel.com.chat;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileFragment extends Fragment {

    @BindView(R.id.ImageUserProfile)
    CircleImageView ImageUser;
    @BindView(R.id.txtUserName)
    TextView UserName;
    @BindView(R.id.btnLogOut)
    Button LogOut;
    @BindView(R.id.ImageActiveProfile)
    ImageView ImageActiveProfile;
    String mOnlineUser;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View ProfilePage = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this,ProfilePage);
        mAuth=FirebaseAuth.getInstance();
        mDatabase= FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);
        mOnlineUser=mAuth.getCurrentUser().getUid();

        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Go_Offline();
                mAuth.signOut();
                startActivity(new Intent(view.getContext(),Login.class));
                getActivity().finish();


            }
        });

        return ProfilePage;
    }
    private void Go_Offline(){
        if (FirebaseAuth.getInstance().getCurrentUser()!=null){
            FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth
                    .getInstance().getCurrentUser().getUid())
                    .child("active")
                    .setValue(String
                            .valueOf(System.currentTimeMillis()));
        }
    }
    private void SetProfileData(String mUserID, final Context context) {
        mDatabase.child("Users").child(mUserID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue()!=null) {

                    String name = (String) dataSnapshot.child("name").getValue();
                    String imageUrl = (String) dataSnapshot.child("image").getValue();
                    UserName.setText(name);
                    Glide.with(context.getApplicationContext()).load(imageUrl).into(ImageUser);

                    String Active = (String) dataSnapshot.child("active").getValue();
                    assert (Active) != null;
                    if ((Active).equals("Yes"))
                        setActive();
                    else
                        setOffline();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    void setActive()
    {
        ImageActiveProfile.setVisibility(View.VISIBLE);
    }
    void setOffline()
    {
        ImageActiveProfile.setVisibility(View.INVISIBLE);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String mUserID = getArguments().getString("ID");
        if (mUserID !=null){
            if (!mUserID.equals(mAuth.getCurrentUser().getUid()))
                LogOut.setVisibility(View.GONE);
            SetProfileData(mUserID,getContext());
        }

    }

}
