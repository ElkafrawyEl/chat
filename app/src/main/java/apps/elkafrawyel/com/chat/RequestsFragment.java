package apps.elkafrawyel.com.chat;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class RequestsFragment extends Fragment {


    String mOnlineUser;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    RecyclerView FriendReqList;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View Requests_Page= inflater.inflate(R.layout.fragment_requests, container, false);
        android.support.v7.widget.Toolbar Reqtoolbar=Requests_Page.findViewById(R.id.Reqtoolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(Reqtoolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("FriendShip " +
                "Notification");
        mAuth=FirebaseAuth.getInstance();
        mDatabase= FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);
        mOnlineUser=mAuth.getCurrentUser().getUid();
        FriendReqList=Requests_Page.findViewById(R.id.RequestsList);
        FriendReqList.setHasFixedSize(true);
        FriendReqList.setLayoutManager(new LinearLayoutManager(container.getContext()));
        FirebaseRecyclerAdapter<Object,FriendRecViewHolder> Requests_Adapter=
                new FirebaseRecyclerAdapter<Object, FriendRecViewHolder>(
                        Object.class,
                        R.layout.friend_req_row,
                        FriendRecViewHolder.class,
                        mDatabase.child("FriendShip Notification").child(mOnlineUser)
                ) {
                    @Override
                    protected void populateViewHolder(FriendRecViewHolder viewHolder, Object model,
                                                      int position) {
                        if (model!=null){
                            final String UserID= getRef(position).getKey();
                            String[] Data=model.toString().split(",");
                            String state=Data[1];
                            String Time=Data[0];
                            viewHolder.setRow(UserID,state,Time,container.getContext());

                            viewHolder.btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mDatabase.child("Friends_Req").child(mOnlineUser).child
                                            (UserID).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            mDatabase.child("Friends").child(mOnlineUser).child
                                                    (UserID).setValue(String.valueOf(System
                                                    .currentTimeMillis()));
                                            mDatabase.child("Friends").child(UserID).child(mOnlineUser)
                                                    .setValue(String.valueOf(System.currentTimeMillis()));


                                            mDatabase.child("FriendShip Notification").child(UserID)
                                                    .child(mOnlineUser).setValue(System
                                                    .currentTimeMillis()+",accept");

                                            mDatabase.child("FriendShip Notification").child(mOnlineUser)
                                                    .child(UserID).setValue(System.currentTimeMillis()+",accept");
                                        }
                                    });
                                }
                            });
                            viewHolder.btnNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mDatabase.child("Friends_Req").child(mOnlineUser).child
                                            (UserID).setValue(null);
                                    mDatabase.child("FriendShip Notification").child(UserID)
                                            .child(mOnlineUser).setValue("refuse,"+System.currentTimeMillis());
                                    mDatabase.child("FriendShip Notification").child(mOnlineUser)
                                            .child(UserID).setValue(null);
                                }
                            });
                        }
                    }
                };
        Requests_Adapter.notifyDataSetChanged();
        FriendReqList.setAdapter(Requests_Adapter);
        return Requests_Page;
    }

    public static class FriendRecViewHolder extends RecyclerView.ViewHolder {
        CircleImageView ImageUserReq;
        TextView UserNameReq;
        Button btnYes,btnNo;
        DatabaseReference mDatabase;
        TextView txtDisplayMessage;
        FirebaseAuth mAuth;
        TextView txtRequestTime;
        public FriendRecViewHolder(View itemView) {
            super(itemView);
            mDatabase= FirebaseDatabase.getInstance().getReference();
            mDatabase.keepSynced(true);
            mAuth=FirebaseAuth.getInstance();
            ImageUserReq=itemView.findViewById(R.id.ImageUserReqDialog);
            UserNameReq=itemView.findViewById(R.id.txtUserNameReqDialog);
            btnNo=itemView.findViewById(R.id.btnNo);
            btnYes=itemView.findViewById(R.id.btnYes);
            txtRequestTime=itemView.findViewById(R.id.txtRequestTime);
            txtDisplayMessage=itemView.findViewById(R.id.txtDisplayMessage);
        }
        void setRow(String ID, String state, final String Time, final Context context){
            switch (state) {
                case "req":
                    mDatabase.child("Users").child(ID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            UserNameReq.setText(dataSnapshot.child("name").getValue().toString());
                            Glide.with(context).load(dataSnapshot.child("image").getValue().toString()).into
                                    (ImageUserReq);
                            Calendar calendar = Calendar.getInstance();
                            DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                            calendar.setTimeInMillis(Long.parseLong(Time));
                            String When= dateFormat.format(calendar.getTime());
                            txtRequestTime.setText(When);
                            txtDisplayMessage.setText("Accept His FriendShip Request ?");
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    break;
                case "un":
                    mDatabase.child("Users").child(ID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            UserNameReq.setText(dataSnapshot.child("name").getValue().toString());
                            Glide.with(context).load(dataSnapshot.child("image").getValue().toString()).into
                                    (ImageUserReq);

                            String Message = UserNameReq.getText() + " No Longer Friend With You";
                            txtDisplayMessage.setText(Message);

                            btnNo.setVisibility(View.GONE);
                            btnYes.setVisibility(View.GONE);
                            Calendar calendar = Calendar.getInstance();
                            DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                            calendar.setTimeInMillis(Long.parseLong(Time));
                            String When= dateFormat.format(calendar.getTime());
                            txtRequestTime.setText(When);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    break;
                case "accept":
                    mDatabase.child("Users").child(ID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            UserNameReq.setText(dataSnapshot.child("name").getValue().toString());
                            Glide.with(context).load(dataSnapshot.child("image").getValue().toString()).into
                                    (ImageUserReq);

                            String Message = "You become friend with "+UserNameReq.getText();
                            txtDisplayMessage.setText(Message);

                            btnNo.setVisibility(View.GONE);
                            btnYes.setVisibility(View.GONE);
                            Calendar calendar = Calendar.getInstance();
                            DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                            calendar.setTimeInMillis(Long.parseLong(Time));
                            String When= dateFormat.format(calendar.getTime());
                            txtRequestTime.setText(When);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    break;
                case "refuse":
                    mDatabase.child("Users").child(ID).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            UserNameReq.setText(dataSnapshot.child("name").getValue().toString());
                            Glide.with(context).load(dataSnapshot.child("image").getValue().toString()).into
                                    (ImageUserReq);

                            String Message = UserNameReq.getText() + " Refuse Your FriendShip Request";
                            txtDisplayMessage.setText(Message);

                            btnNo.setVisibility(View.GONE);
                            btnYes.setVisibility(View.GONE);
                            Calendar calendar = Calendar.getInstance();
                            DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                            calendar.setTimeInMillis(Long.parseLong(Time));
                            String When= dateFormat.format(calendar.getTime());
                            txtRequestTime.setText(When);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    break;
            }
        }

    }
    //================================================================================

}
