package apps.elkafrawyel.com.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class RoomsFragment extends Fragment {

    @BindView(R.id.myAddMessageFAB)
    FloatingActionButton myFloatingActionButton;
    @BindView(R.id.RoomsList)
    RecyclerView RoomsList;
    @BindView(R.id.Roomtoolbar)
    android.support.v7.widget.Toolbar Roomtoolbar;
    DatabaseReference mDatabase;
    FirebaseAuth mAuth;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View RoomsPage= inflater.inflate(R.layout.fragment_rooms, container, false);
        ButterKnife.bind(this,RoomsPage);
        ((AppCompatActivity) getActivity()).setSupportActionBar(Roomtoolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Latest Messages");
        RoomsList.setLayoutManager(new LinearLayoutManager(container.getContext()));
        RoomsList.setHasFixedSize(true);
        mDatabase= FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);
        mAuth=FirebaseAuth.getInstance();

        myFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenMultiMessageDialog();
            }
        });
        return  RoomsPage;
    }

    private void OpenMultiMessageDialog() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        GetMyRooms();
    }

    private void GetMyRooms(){
        FirebaseRecyclerAdapter<String, MessagesHistory_ViewHolder> Messages_Adapter;
        Query M_Query=mDatabase.child("Messages Notification Center")
                .child(mAuth.getCurrentUser().getUid()).orderByValue();
        Messages_Adapter=new FirebaseRecyclerAdapter<String, MessagesHistory_ViewHolder>(
                String.class,
                R.layout.room_row,
                MessagesHistory_ViewHolder.class,
                M_Query
        ) {

            @Override
            public MessagesHistory_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new MessagesHistory_ViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.room_row, parent, false));
            }
            @Override
            protected void populateViewHolder(MessagesHistory_ViewHolder viewHolder, final String
                    MessageID, int position) {
                try {
                    if (MessageID != null) {
                        final String RoomId = getRef(position).getKey();
                        viewHolder.SetCommingRow(MessageID, RoomId, getContext());
                        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                FirebaseDatabase.getInstance().getReference().child("Messages")
                                        .child(RoomId).child(mAuth.getCurrentUser().getUid()).child(MessageID)
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(final DataSnapshot dataSnapshot) {
                                                message m = dataSnapshot.getValue(message.class);

                                                assert m != null;
                                                if (m.getFrom().equals(mAuth.getCurrentUser().getUid()))
                                                    m.setFrom(m.getTo());

                                                FirebaseDatabase.getInstance().getReference().child("Users")
                                                        .child(m.getFrom()).addListenerForSingleValueEvent(
                                                        new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot Snapshot) {
                                                                String ID = Snapshot.getKey();
                                                                String RoomID = Snapshot.child("Room_IDs").child(mAuth.getCurrentUser().getUid()).getValue().toString();
                                                                //Open Chat with him
                                                                OpenChat(ID, RoomID);
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {

                                                            }
                                                        }
                                                );
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        RoomsList.setAdapter(Messages_Adapter);
    }
    private void OpenChat(final String MyFriendID,String RoomID){
        Intent i=new Intent(getContext(),Chatting_Room.class);
        i.putExtra("ID",MyFriendID);
        i.putExtra("RoomID",RoomID);
        getContext().startActivity(i);
    }
    static class MessagesHistory_ViewHolder extends RecyclerView.ViewHolder{
        View mView;
        CircleImageView ImageUser;
        TextView txtUserName,txtMessage,txtTime;
        DatabaseReference mDatabase;
        FirebaseAuth mAuth;
        MessagesHistory_ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            ImageUser=mView.findViewById(R.id.ImageUsersRoom);
            txtUserName=mView.findViewById(R.id.txtUsersNameRoom);
            txtMessage=mView.findViewById(R.id.txtMessageRoom);
            txtTime=mView.findViewById(R.id.txtMessageTimeRoom);
            mDatabase=FirebaseDatabase.getInstance().getReference();
            mDatabase.keepSynced(true);
            mAuth=FirebaseAuth.getInstance();
        }

        void SetCommingRow(String MessageID,String Room,final Context context){

            //Message Data
            mDatabase.child("Messages")
                    .child(Room).child(mAuth.getCurrentUser().getUid()).child(MessageID)
                    .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue()!=null){
                        message m=dataSnapshot.getValue(message.class);
                        if (m!=null){
                            //set text and time
                            Calendar calendar = Calendar.getInstance();
                            DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                            calendar.setTimeInMillis(Long.parseLong(m.getWhen()));
                            String Time= dateFormat.format(calendar.getTime());
                            txtTime.setText(Time);
                            if (m.getType().equals("text")){
                                if (m.getFrom().equals(mAuth.getCurrentUser().getUid())) {
                                    txtMessage.setText("You :"+m.getMessage());
                                    m.setFrom(m.getTo());
                                }else {
                                    txtMessage.setText(m.getMessage());
                                }
                            }else if (m.getType().equals("image")){
                                if (m.getFrom().equals(mAuth.getCurrentUser().getUid())) {
                                    txtMessage.setText("You sent an image");
                                    m.setFrom(m.getTo());
                                }else {
                                    txtMessage.setText("You received an image");
                                }
                            }else if (m.getType().equals("voice")){
                                if (m.getFrom().equals(mAuth.getCurrentUser().getUid())) {
                                    txtMessage.setText("You sent a voice clip");
                                    m.setFrom(m.getTo());
                                }else {
                                    txtMessage.setText("You received a voice clip");
                                }
                            }
                            //User Data
                            mDatabase.child("Users").child(m.getFrom()).addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                                                   @Override
                                                                                                                   public void onDataChange(DataSnapshot dataSnapshot) {
                                                                                                                       //Set name ,image
                                                                                                                       String Name=dataSnapshot.child("name").getValue().toString();
                                                                                                                       String Image=dataSnapshot.child("image").getValue().toString();
                                                                                                                       txtUserName.setText(Name);
                                                                                                                       Glide.with(context.getApplicationContext()).load(Image).into(ImageUser);
                                                                                                                   }
                                                                                                                   @Override
                                                                                                                   public void onCancelled(DatabaseError databaseError) {
                                                                                                                   }
                                                                                                               });
                        }
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });

        }
    }



}
