package apps.elkafrawyel.com.chat;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SingUp extends AppCompatActivity {
    @BindView(R.id.mSignupToolBar)Toolbar mSignupToolBar;
    @BindView(R.id.Reg_txtUsername)TextView txtName;
    @BindView(R.id.Reg_txtEmail)TextView txtEmail;
    @BindView(R.id.Reg_txtPassword)TextView txtPassword;
    @BindView(R.id.Reg_btnRegister)Button btnRegister;
    @BindView(R.id.Reg_btnCancel)Button btnCancel;
    @BindView(R.id.SingUpProgressBar)ProgressBar progressBar;
    @BindView(R.id.ImageUser)CircleImageView ImageUser;
    String mName,mEmail,mPassword;

    FirebaseAuth mAuth;
    DatabaseReference mUsersDatabase;
    StorageReference mStorageReference;
    private final int GalleryRequest=100;
    private Uri ImageUri=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        mAuth=FirebaseAuth.getInstance();
        mUsersDatabase= FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);
        mStorageReference= FirebaseStorage.getInstance().getReference().child("Images");
        ButterKnife.bind(this);

        setSupportActionBar(mSignupToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
        ImageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mGalleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                mGalleryIntent.setType("image/*");
                startActivityForResult(mGalleryIntent, GalleryRequest);
            }
        });
    }

    public boolean isNetworkAvailable(Context C){
        ConnectivityManager mConnectivity = (ConnectivityManager) C.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert mConnectivity != null;
        NetworkInfo networkInfo = mConnectivity.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public void Register(View view) {
        if (isNetworkAvailable(this)) {
            mName = txtName.getText().toString();
            mEmail = txtEmail.getText().toString();
            mPassword = txtPassword.getText().toString();
            if (!TextUtils.isEmpty(mName)
                    && !TextUtils.isEmpty(mEmail)
                    && !TextUtils.isEmpty(mPassword)) {
                if (mAuth != null&&isNetworkAvailable(SingUp.this)) {
                    if (ImageUri != null) {
                        progressBar.setVisibility(View.VISIBLE);
                        mAuth.createUserWithEmailAndPassword(mEmail, mPassword)
                                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(final AuthResult authResult) {
                                        final StorageReference newUsersRef = mStorageReference.child
                                                (authResult.getUser().getUid() + ".jpg");
                                        newUsersRef.putFile(ImageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                                String Url = String.valueOf(task.getResult()
                                                        .getDownloadUrl());

                                                if (task.isSuccessful() && Url != null) {
                                                    progressBar.setVisibility(View.INVISIBLE);

                                                    mUsersDatabase.child(authResult.getUser()
                                                            .getUid()).child("name").setValue
                                                            (mName);
                                                    mUsersDatabase.child(authResult.getUser()
                                                            .getUid()).child("image").setValue(Url);
                                                    mUsersDatabase.child(authResult.getUser()
                                                            .getUid()).child("active").setValue("Yes");

                                                    mUsersDatabase.child(authResult.getUser()
                                                            .getUid()).child("typing").setValue(false);
                                                    Intent i = new Intent(SingUp.this, MainActivity.class);
                                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(i);
                                                    finish();
                                                    progressBar.setVisibility(View.INVISIBLE);

                                                }
                                            }
                                        });
                                    }
                                });
                    }
                } else {
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(SingUp.this, "Make Sure to complete all fields", Toast.LENGTH_SHORT).show();
                }
            } else
                Toast.makeText(SingUp.this, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==GalleryRequest &&resultCode==RESULT_OK){
            ImageUri=data.getData();
            ImageUser.setImageURI(ImageUri);
        }
    }

    public void CancelClicked(View view) {
        finish();
    }
}
