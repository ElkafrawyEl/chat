package apps.elkafrawyel.com.chat;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Mahmoud Ashraf on 1/16/2018.
 */

public class chat extends Application  {
    private FirebaseAuth mAuth;
    @Override
    public void onCreate() {
        super.onCreate();
        //To allow offline storage only Strings allowed
        if (!FirebaseApp.getApps(this).isEmpty()){
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            FirebaseDatabase.getInstance().getReference().keepSynced(true);
        }
        mAuth=FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser()!=null){
            DatabaseReference myRef=FirebaseDatabase.getInstance().getReference().child("Users");
            myRef.child(mAuth.getCurrentUser().getUid()).child("active").setValue("Yes");
        }

        commonMethods.GoOnline();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    CommonMethods commonMethods=new CommonMethods();
    @Override
    public void onTerminate() {
        super.onTerminate();
        commonMethods.Go_Offline();
    }
}
