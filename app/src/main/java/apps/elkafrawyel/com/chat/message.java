package apps.elkafrawyel.com.chat;

/**
 * Created by Mahmoud Ashraf on 1/17/2018.
 */

public class message {
    private String message,type,from,to,seen,when;
    public message() {
    }

    public message(String message, String type, String from, String to, String seen, String when) {
        this.message = message;
        this.type = type;
        this.from = from;
        this.to = to;
        this.seen = seen;
        this.when = when;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }
}
