package apps.elkafrawyel.com.chat;

/**
 * Created by Mahmoud Ashraf on 1/16/2018.
 */

public class users {
    private String name;
    private String image;
    private String active;
    private boolean typing;
    public users() {
    }

    public users(String name, String image, String active, boolean typing) {
        this.name = name;
        this.image = image;
        this.active = active;
        this.typing = typing;
    }

    public boolean isTyping() {
        return typing;
    }

    public void setTyping(boolean typing) {
        this.typing = typing;
    }

    String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
